// Need to fix the joining through shared link
// disabled the create and dont do what it does now

(function() {
  var params = {},
    r = /([^&=]+)=?([^&]*)/g,
    match, search = window.location.search;

  while (match = r.exec(search.substring(1)))
    params[tools.deodeURI(match[1])] = tools.deodeURI(match[2]);
  window.params = params;
})();

var roomid = '',
  hashString = location.hash.replace('#', ''),
  connection = new RTCMultiConnection();

// toolbox
var tools = {
  getRandomArbitrary: function(min, max) {
    return Math.random() * (max - min) + min;
  },
  generateBubbles: function() {
    for(let i = 0; i < tools.getRandomArbitrary(6, 10); i++) {
      $('.bg-bubbles').append('<li></li>');
    }
  },
  renderVoiceSection: function() {
    setTimeout(function(){
      $('#audios-container').css({display: 'flex'});
      $('#audios-container').fadeIn(300);
      $('.x').fadeIn(300);
    }, 4000);
  },
  showRoomURL: function(roomid) {
    var html = '<h1>Room Id:</h1>';
    html += '<div class="room-id" title="Click To Copy">' + roomid + 
      '<span><img class="copy-ico" src="./assets/copy.png" width="30px"/></span></div>';

    window.location.href = '#' + roomid;
    var roomURLsDiv = document.getElementById('room-urls');
    roomURLsDiv.innerHTML = html;

    roomURLsDiv.style.display = 'block';
    
    //copying comes here
    $('.room-id').attr('data-clipboard-text', window.location.href);
    tools.copyUrl();
  },
  deodeURI: function(aString) {
    return decodeURIComponent(aString.replace(/\+/g, ' '));
  },
  userJoinedSharedLink: function () {
    if(window.location.hash) {
      $('#open-room').hide();
    }
  },
  copyUrl: function () {
    var clipboard = new Clipboard('.room-id');

    $('.room-id').hover(function(){
      $('.copy-ico').fadeIn(500);
    }, function(){
      $('.copy-ico').fadeOut(500);
    });

    //callback after the copy has taken place
    clipboard.on('success', function(e) {
      $('.copied-notification').fadeIn(500).delay(300).fadeOut(500);
    });
  },
  camelize: function(str) {
    return str.replace(/(?:^\w|[A-Z]|\b\w)/g, function(letter, index) {
      return index == 0 ? letter.toLowerCase() : letter.toUpperCase();
    }).replace(/\s+/g, '');
  }
}

// render bubbles
tools.generateBubbles();

//check if user joined through shared link
tools.userJoinedSharedLink();

// --------------------------
// ------ Click Events ------
// --------------------------

$("#open-room, #join-room").click(function(event){
    event.preventDefault();
  
  $('form').fadeOut(500);
  $('.wrapper').addClass('form-success');

  //show loader
  setTimeout(function(){
    $('.loaderGif').fadeIn().delay(1500).fadeOut(3000);
    $('.container').delay(1500).fadeOut(2000);
    tools.renderVoiceSection();
  },1500);
});

$('#open-room').on('click' , function() {
  connection.open(tools.camelize($('#room-id').val()), function() {
      tools.showRoomURL(connection.sessionid);
  });
});

$('#join-room').on('click', function() {
    connection.join(tools.camelize($('#room-id').val()), function() {
      tools.showRoomURL(connection.sessionid);
  });
});

$('.x').on('click' , function() {
  connection.leave();
  window.location.href = window.location.origin
});

// -------------------------------------
// ------ RTCMultiConnection Code ------
// -------------------------------------

// by default, socket.io server is assumed to be deployed on your own URL
connection.socketURL = '/';
// comment-out below line if you do not have your own socket.io server (GET OWN SOCKET SERVER FOR BELOW)
connection.socketURL = 'https://rtcmulticonnection.herokuapp.com:443/';
connection.socketMessageEvent = 'ET-Voice';

connection.session = {
    audio: true,
    video: false
};

connection.mediaConstraints = {
    audio: true,
    video: false
};

connection.sdpConstraints.mandatory = {
    OfferToReceiveAudio: true,
    OfferToReceiveVideo: false
};

connection.audiosContainer = document.getElementById('audios-container');
connection.onstream = function(event) {
    var width = parseInt(connection.audiosContainer.clientWidth / 2) - 20;
    var mediaElement = event.mediaElement;

    connection.audiosContainer.appendChild(mediaElement);
    mediaElement.media.play();
    mediaElement.id = event.streamid;
};

connection.onstreamended = function(event) {
    var mediaElement = document.getElementById(event.streamid);
    if (mediaElement) {
        mediaElement.parentNode.removeChild(mediaElement);
    }
};

if (localStorage.getItem(connection.socketMessageEvent)) {
  roomid = localStorage.getItem(connection.socketMessageEvent);
} else {
  roomid = connection.token();
}

$('#room-id').val(roomid);
$('#room-id').on('onkeyup', function() {
    localStorage.setItem(connection.socketMessageEvent, this.value);
});

if (hashString.length && hashString.indexOf('comment-') == 0) {
  hashString = '';
}

roomid = params.roomid;
if (!roomid && hashString.length) {
  roomid = hashString;
}

if (roomid && roomid.length) {
  $('#room-id').val(roomid);
  localStorage.setItem(connection.socketMessageEvent, roomid);
  // auto-join-room
  (function reCheckRoomPresence() {
    connection.checkPresence(roomid, function(isRoomExist) {
      if (isRoomExist) {
          connection.join(roomid);
          return;
      }

      setTimeout(reCheckRoomPresence, 5000);
    });
  })();
}